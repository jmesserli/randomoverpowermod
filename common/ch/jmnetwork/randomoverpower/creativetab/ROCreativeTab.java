package ch.jmnetwork.randomoverpower.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StringTranslate;
import ch.jmnetwork.randomoverpower.items.ItemReference;
import ch.jmnetwork.randomoverpower.items.Items;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ROCreativeTab extends CreativeTabs {

	public ROCreativeTab(int creativeTabID, String ctabLabel) {
		super(creativeTabID, ctabLabel);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getTabIconItemIndex() {
		return ItemReference.HARDDIAMOND_DEFAULT_ID;
	}

	@Override
	public ItemStack getIconItemStack() {
		return new ItemStack(Items.hardDiamond);
	}

	@Override
	public String getTranslatedTabLabel() {
		return getTabLabel();
	}

	@Override
	public String getTabLabel() {
		return "Random Overpower Mod";
	}

}
