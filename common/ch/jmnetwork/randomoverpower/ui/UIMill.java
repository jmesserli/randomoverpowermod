package ch.jmnetwork.randomoverpower.ui;

import org.lwjgl.opengl.GL11;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityMill;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;


/**
 * @author Joel Messerli
 */
public class UIMill extends GuiContainer {

	public static final ResourceLocation texture = new ResourceLocation("randomoverpwr", "textures/ui/mill.png");

	public UIMill(InventoryPlayer invPlayer, TileEntityMill mill) {

		super(new ContainerMill(invPlayer, mill));

		xSize = 176;
		ySize = 166;

	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {

		GL11.glColor4f(1, 1, 1, 1);

		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}

}
