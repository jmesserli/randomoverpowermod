package ch.jmnetwork.randomoverpower.ui;

import ch.jmnetwork.randomoverpower.RandomOverpowerMod;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityMill;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityPVoidStorage;
import ch.jmnetwork.randomoverpower.items.ItemPortableVoid;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;


public class GUIHandler implements IGuiHandler {

	public GUIHandler() {

		NetworkRegistry.instance().registerGuiHandler(RandomOverpowerMod.INSTANCE, this);
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

		switch (ID) {
		case 0:
			TileEntity te = world.getBlockTileEntity(x, y, z);
			if (te != null && te instanceof TileEntityMill) return new ContainerMill(player.inventory, (TileEntityMill) te);
			break;
		case 1:
			TileEntity te1 = world.getBlockTileEntity(x, y, z);
			return new ContainerPortableVoid(player.inventory, (TileEntityPVoidStorage) te1);
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

		switch (ID) {
		case 0:
			TileEntity te = world.getBlockTileEntity(x, y, z);
			if (te != null && te instanceof TileEntityMill) return new UIMill(player.inventory, (TileEntityMill) te);
			break;
		case 1:
			TileEntity te1 = world.getBlockTileEntity(x, y, z);
			return new UIPortableVoid(player.inventory, (TileEntityPVoidStorage) te1);
		}

		return null;
	}

}
