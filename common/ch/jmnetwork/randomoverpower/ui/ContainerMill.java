package ch.jmnetwork.randomoverpower.ui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityMill;

public class ContainerMill extends Container {

	private TileEntityMill mill;

	public ContainerMill(InventoryPlayer invPlayer, TileEntityMill mill) {
		this.mill = mill;

		// ======================================//
		// PLAYER INVENTORY SLOTS
		// ======================================//

		for (int x = 0; x < 9; x++) {
			addSlotToContainer(new Slot(invPlayer, x, 8 + 18 * x, 142));
		}

		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 9; x++) {
				addSlotToContainer(new Slot(invPlayer, x + y * 9 + 9, 8 + 18 * x, 84 + y * 18));
			}
		}

		// ======================================//
		// MILL SLOTS
		// ======================================//

		addSlotToContainer(new SlotMill(mill, 0, 66, 35));
		addSlotToContainer(new SlotMill(mill, 1, 124, 35));
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return mill.isUseableByPlayer(player);
	}

	// TODO: make shiftClick work
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotID) {
		return null;
	}

}
