package ch.jmnetwork.randomoverpower.ui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;


public class UIPortableVoid extends GuiContainer {

	public UIPortableVoid(InventoryPlayer invPlayer, IInventory pvoid) {

		super(new ContainerPortableVoid(invPlayer, pvoid));

		xSize = 176;
		ySize = 145;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {

		GL11.glColor4f(1, 1, 1, 1);

		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("randomoverpwr", "textures/ui/guiPortableVoid.png"));
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}

}
