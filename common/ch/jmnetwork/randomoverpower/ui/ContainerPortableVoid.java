package ch.jmnetwork.randomoverpower.ui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerPortableVoid extends Container {

	public ContainerPortableVoid(InventoryPlayer invPlayer, IInventory pvoid) {
		// ======================================//
		// PLAYER INVENTORY SLOTS
		// ======================================//

		for (int x = 0; x < 9; x++) {
			addSlotToContainer(new Slot(invPlayer, x, 8 + 18 * x, 121));
		}

		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 9; x++) {
				addSlotToContainer(new Slot(invPlayer, x + y * 9 + 9, 8 + 18 * x, 63 + y * 18));
			}
		}

		// ======================================//
		// PORTABLE VOID INVENTORY SLOTS
		// ======================================//

		for (int x = 0; x < 9; x++) {
			addSlotToContainer(new Slot(pvoid, x, 8 + 18 * x, 18));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return true;
	}

	// TODO: make shiftClick work
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotID) {
		return null;
	}

}
