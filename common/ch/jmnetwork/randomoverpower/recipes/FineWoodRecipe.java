package ch.jmnetwork.randomoverpower.recipes;

import ch.jmnetwork.randomoverpower.RandomOverpowerMod;
import ch.jmnetwork.randomoverpower.items.Items;
import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;

public class FineWoodRecipe implements IRecipe {

	@Override
	public boolean matches(InventoryCrafting inventorycrafting, World world) {
		ItemStack is1 = inventorycrafting.getStackInSlot(0);
		ItemStack is2 = inventorycrafting.getStackInSlot(3);
		boolean returnValue = (is1 != null && (is1.itemID == Item.axeWood.itemID || is1.itemID == Item.axeStone.itemID || is1.itemID == Item.axeIron.itemID || is1.itemID == Item.axeGold.itemID || is1.itemID == Item.axeDiamond.itemID || is1.itemID == Items.overpowerAxe.itemID)) && (is2 != null && (is2.itemID == Block.wood.blockID));
		return returnValue;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inventorycrafting) {
		return new ItemStack(Items.fineWood, 9);
	}

	@Override
	public int getRecipeSize() {
		return 4;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return null;
	}

}
