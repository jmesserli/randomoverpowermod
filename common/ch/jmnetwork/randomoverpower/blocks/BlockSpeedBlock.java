package ch.jmnetwork.randomoverpower.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ch.jmnetwork.randomoverpower.lib.Reference;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class BlockSpeedBlock extends Block {

	public BlockSpeedBlock(int par1) {
		super(par1, BlockReference.SPEEDBLOCK_MATERIAL);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
		setUnlocalizedName(BlockReference.SPEEDBLOCK_UNLOCALIZED_NAME);
		setLightValue(1.0F);
		setHardness(1.0F);
	}

	@Override
	public void onEntityWalking(World world, int par2, int par3, int par4, Entity entity) {
		double boost = 4D;

		double mX = Math.abs(entity.motionX);
		double mZ = Math.abs(entity.motionZ);
		if (mX < 1D) {
			entity.motionX *= boost;
		}
		if (mZ < 1D) {
			entity.motionZ *= boost;
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister par1IconRegister) {
		this.blockIcon = par1IconRegister.registerIcon(Reference.jutil.getTextureString(BlockReference.SPEEDBLOCK_ICON));
	}
}
