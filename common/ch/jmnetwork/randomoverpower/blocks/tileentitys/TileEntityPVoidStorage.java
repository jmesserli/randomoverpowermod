package ch.jmnetwork.randomoverpower.blocks.tileentitys;

import java.util.Random;
import com.google.common.collect.SetMultimap;
import cpw.mods.fml.common.Mod.Metadata;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;


public class TileEntityPVoidStorage extends TileEntity implements IInventory {

	ItemStack items[];
	int ID;

	public TileEntityPVoidStorage() {

		items = new ItemStack[9];
		ID = new Random().nextInt(100000);
		System.out.println(ID);
	}

	public int getID() {

		return ID;
	}

	@Override
	public int getSizeInventory() {

		return items.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {

		return items[i];
	}

	@Override
	public ItemStack decrStackSize(int i, int count) {

		ItemStack item = getStackInSlot(i);

		if (item != null) {
			if (item.stackSize <= count) {
				setInventorySlotContents(i, null);
			} else {
				item = item.splitStack(count);
				onInventoryChanged();
			}
		}
		return item;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {

		ItemStack item = getStackInSlot(i);

		setInventorySlotContents(i, null);

		return item;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {

		if (itemstack != null && itemstack.stackSize > getInventoryStackLimit()) {
			itemstack.stackSize = getInventoryStackLimit();
		}

		items[i] = itemstack;

		onInventoryChanged();
	}

	@Override
	public String getInvName() {

		return "Portable Void Storage";
	}

	@Override
	public boolean isInvNameLocalized() {

		return false;
	}

	@Override
	public int getInventoryStackLimit() {

		return 1;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {

		return true;
	}

	@Override
	public void openChest() {

	}

	@Override
	public void closeChest() {

	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {

		return true;
	}

	@Override
	public void onInventoryChanged() {

	}

	@Override
	public void writeToNBT(NBTTagCompound compound) {

		super.writeToNBT(compound);

		NBTTagList items = new NBTTagList();

		for (int i = 0; i < getSizeInventory(); i++) {
			ItemStack stack = getStackInSlot(i);

			if (stack != null) {
				NBTTagCompound item = new NBTTagCompound();
				item.setByte("Slot", (byte) i);
				stack.writeToNBT(item);
				items.appendTag(item);
			}
		}

		compound.setTag("Items", items);
		compound.setInteger("ID_", ID);
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {

		super.readFromNBT(compound);

		NBTTagList items = compound.getTagList("Items");

		for (int i = 0; items.tagCount() > i; i++) {

			NBTTagCompound item = (NBTTagCompound) items.tagAt(i);
			int slot = item.getByte("Slot");

			if (slot >= 0 && slot < getSizeInventory()) {
				setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(item));
			}
		}
		ID = compound.getInteger("ID_");
	}
}
