package ch.jmnetwork.randomoverpower.blocks;

import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityMill;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityPVoidStorage;
import ch.jmnetwork.randomoverpower.util.MultiLanguageRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;


public class Blocks {

	MultiLanguageRegistry mlr = new MultiLanguageRegistry();

	Block glowCobble;
	Block speedBlock;
	Block blockMill;
	Block blockPVoidStorage;

	public void init() {

		if (BlockReference.GLOWCOBBLE_ACTIVE) {
			glowCobble = new BlockGlowCobble(BlockReference.GLOWCOBBLE_DEFAULT_ID, Material.rock).setUnlocalizedName(BlockReference.GLOWCOBBLE_UNLOCALIZED_NAME);
			MinecraftForge.setBlockHarvestLevel(glowCobble, "pickaxe", 1);
			GameRegistry.registerBlock(glowCobble, ItemBlock.class, "glowCobble");
		}
		if (BlockReference.SPEEDBLOCK_ACTIVE) {
			speedBlock = new BlockSpeedBlock(BlockReference.SPEEDBLOCK_DEFAULT_ID);
			MinecraftForge.setBlockHarvestLevel(speedBlock, "pickaxe", 1);
			GameRegistry.registerBlock(speedBlock, ItemBlock.class, "SPEEDBLOCK");
		}
		if (BlockReference.MILL_ACTIVE) {
			blockMill = new BlockMill(BlockReference.MILL_DEFAULT_ID, BlockReference.MILL_MATERIAL);
			MinecraftForge.setBlockHarvestLevel(blockMill, "pickaxe", 1);
			GameRegistry.registerBlock(blockMill, ItemBlock.class, "blockMill");
		}
		if (BlockReference.PVOID_INV_ACTIVE) {
			blockPVoidStorage = new BlockPVoidInvStorage(BlockReference.PVOID_INV_DEFAULT_ID, Material.iron);
			MinecraftForge.setBlockHarvestLevel(blockPVoidStorage, "pickaxe", 1);
			GameRegistry.registerBlock(blockPVoidStorage, ItemBlock.class, "blockPvoidStorage");
		}
	}

	public void addNames() {

		if (BlockReference.GLOWCOBBLE_ACTIVE) {
			mlr.nameObject(glowCobble, BlockReference.GLOWCOBBLE_NAME_EN, BlockReference.GLOWCOBBLE_NAME_DE);
		}
		if (BlockReference.SPEEDBLOCK_ACTIVE) {
			mlr.nameObject(speedBlock, BlockReference.SPEEDBLOCK_NAME_EN, BlockReference.SPEEDBLOCK_NAME_DE);
		}
		if (BlockReference.MILL_ACTIVE) {
			mlr.nameObject(blockMill, BlockReference.MILL_NAME_EN, BlockReference.MILL_NAME_DE);
		}
		if (BlockReference.PVOID_INV_ACTIVE) {
			mlr.nameObject(blockPVoidStorage, BlockReference.PVOID_INV_NAME_EN, BlockReference.PVOID_INV_NAME_DE);
		}
	}

	public void addRecipes() {

		if (BlockReference.GLOWCOBBLE_ACTIVE) {
			GameRegistry.addRecipe(new ItemStack(glowCobble, 8), new Object[] { "CCC", "CGC", "CCC", 'C', Block.cobblestone, 'G', Block.glowStone });
		}
		if (BlockReference.SPEEDBLOCK_ACTIVE) {
			GameRegistry.addRecipe(new ItemStack(speedBlock, 1), "S", "C", 'S', Item.slimeBall, 'C', glowCobble);
		}
		if (BlockReference.PVOID_INV_ACTIVE) {
			GameRegistry.addRecipe(new ItemStack(blockPVoidStorage), new Object[] { "ESE", "SRS", "ESE", 'E', Item.enderPearl, 'S', Block.stone, 'R', Item.redstone });
		}
	}

	public void regTileEntitys() {

		if (BlockReference.MILL_ACTIVE) {
			GameRegistry.registerTileEntity(TileEntityMill.class, "TE_Mill");
		}
		if (BlockReference.PVOID_INV_ACTIVE) {
			GameRegistry.registerTileEntity(TileEntityPVoidStorage.class, "TE_PVOID_STORAGE");
		}
	}
}
