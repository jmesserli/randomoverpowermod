package ch.jmnetwork.randomoverpower.blocks;

import ch.jmnetwork.randomoverpower.lib.Reference;
import net.minecraft.block.material.Material;


public class BlockReference {

	// public static final String VARIABLE = "";

	public static final String TEXTURE_LOC = Reference.MOD_ID;

	public final static String GLOWCOBBLE_NAME_EN = "Glowstone Infused Cobble";
	public final static String GLOWCOBBLE_NAME_DE = "Gluehender Cobblestein";
	public final static String GLOWCOBBLE_UNLOCALIZED_NAME = "glowCobble";
	public final static int GLOWCOBBLE_DEFAULT_ID = 1005;
	public final static String GLOWCOBBLE_ICON = "glowCobble";
	public static Boolean GLOWCOBBLE_ACTIVE = true;

	public final static String SPEEDBLOCK_NAME_EN = "Speed Block";
	public final static String SPEEDBLOCK_NAME_DE = "Geschwindikeitsblock";
	public final static String SPEEDBLOCK_UNLOCALIZED_NAME = "speedBlock";
	public final static int SPEEDBLOCK_DEFAULT_ID = 1006;
	public final static Material SPEEDBLOCK_MATERIAL = Material.rock;
	public final static String SPEEDBLOCK_ICON = "speedBlock";
	public static Boolean SPEEDBLOCK_ACTIVE = true;

	public final static String MILL_NAME_EN = "Mill";
	public final static String MILL_NAME_DE = "Muehle";
	public final static String MILL_UNLOCALIZED_NAME = "blockMill";
	public final static String MILL_ICON = "blockMill";
	public final static int MILL_DEFAULT_ID = 1007;
	public final static Material MILL_MATERIAL = Material.iron;
	public static Boolean MILL_ACTIVE = true;

	public final static String PVOID_INV_NAME_EN = "Portable Void Inventory Storage";
	public final static String PVOID_INV_NAME_DE = "Tragbares Schwarzes Loch Inventar Speicher";
	public final static String PVOID_INV_UNLOCALIZED_NAME = "pvoidInvBlock";
	public final static int PVOID_INV_DEFAULT_ID = 1008;
	public final static String PVOID_INV_ICON = "pvoidInvBlock";
	public static Boolean PVOID_INV_ACTIVE = true;
}
