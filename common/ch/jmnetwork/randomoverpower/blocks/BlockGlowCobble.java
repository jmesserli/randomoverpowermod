package ch.jmnetwork.randomoverpower.blocks;

import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;

public class BlockGlowCobble extends Block {

	public BlockGlowCobble(int par1, Material par2Material) {
		super(par1, par2Material);
		this.setLightValue(1.0F);
		this.setHardness(1.0F);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister par1IconRegister) {
		this.blockIcon = par1IconRegister.registerIcon(Reference.jutil.getTextureString(BlockReference.GLOWCOBBLE_ICON));
	}

}
