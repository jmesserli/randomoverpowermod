package ch.jmnetwork.randomoverpower.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityPVoidStorage;
import ch.jmnetwork.randomoverpower.items.ItemPortableVoid;
import ch.jmnetwork.randomoverpower.items.Items;
import ch.jmnetwork.randomoverpower.lib.Reference;
import ch.jmnetwork.randomoverpower.util.JUtil;


public class BlockPVoidInvStorage extends BlockContainer {

	public BlockPVoidInvStorage(int blockId, Material material) {

		super(blockId, material);
		setUnlocalizedName(BlockReference.PVOID_INV_UNLOCALIZED_NAME);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
	}

	@Override
	public void registerIcons(IconRegister par1IconRegister) {

		blockIcon = par1IconRegister.registerIcon(Reference.jutil.getTextureString(BlockReference.PVOID_INV_ICON));
	}

	@Override
	public TileEntity createNewTileEntity(World world) {

		return new TileEntityPVoidStorage();
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer entityPlayer, int par6, float par7, float par8, float par9) {

		ItemStack is = entityPlayer.inventory.getCurrentItem();
		TileEntityPVoidStorage tepvs = (TileEntityPVoidStorage) world.getBlockTileEntity(x, y, z);

		if (is != null && is.itemID == Items.portableVoid.itemID) {
			is.stackTagCompound.setIntArray("storageCoords", new int[] { x, y, z });
		}

		return true;
	}
}
