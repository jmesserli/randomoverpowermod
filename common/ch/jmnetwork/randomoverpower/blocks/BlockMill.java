package ch.jmnetwork.randomoverpower.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import ch.jmnetwork.randomoverpower.RandomOverpowerMod;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityMill;
import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.common.network.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockMill extends BlockContainer {

	public BlockMill(int id, Material material) {
		super(id, material);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
		this.setHardness(1.0F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		this.blockIcon = register.registerIcon(Reference.jutil.getTextureString(BlockReference.MILL_ICON));
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityMill();
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {

		if (!world.isRemote) {
			FMLNetworkHandler.openGui(player, RandomOverpowerMod.INSTANCE, 0, world, x, y, z);
		}

		return true;
	}
}
