package ch.jmnetwork.randomoverpower;

import ch.jmnetwork.randomoverpower.blocks.Blocks;
import ch.jmnetwork.randomoverpower.items.Items;
import ch.jmnetwork.randomoverpower.lib.Reference;
import ch.jmnetwork.randomoverpower.ui.GUIHandler;
import ch.jmnetwork.randomoverpower.util.ConfigHandler;
import ch.jmnetwork.randomoverpower.util.JPlayerHandler;
import ch.jmnetwork.randomoverpower.util.JUtil;
import ch.jmnetwork.randomoverpower.util.OPCraftHandler;
import ch.jmnetwork.randomoverpower.util.OPFuelHandler;
import ch.jmnetwork.randomoverpower.util.ROPickupHandler;
import ch.jmnetwork.randomoverpower.util.RandomPacketHandler;
import ch.jmnetwork.randomoverpower.world.JWorldGenerator;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin.MCVersion;


@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION)
@NetworkMod(channels = { "RandomMod" }, clientSideRequired = true, serverSideRequired = false, packetHandler = RandomPacketHandler.class)
@MCVersion("1.6.4")
public class RandomOverpowerMod {

	@Instance(Reference.MOD_ID) public static RandomOverpowerMod INSTANCE;

	JUtil jutil = Reference.jutil;

	Blocks blocks = new Blocks();
	Items items = new Items();

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {

		new ConfigHandler(event.getSuggestedConfigurationFile());

	}

	@EventHandler
	public void init(FMLInitializationEvent event) {

		new JWorldGenerator();
		new JPlayerHandler();
		new OPCraftHandler();
		new OPFuelHandler();
		new GUIHandler();
		new ROPickupHandler();

		blocks.init();
		items.init();

		blocks.addNames();
		items.addNames();

		blocks.addRecipes();
		items.addRecipes();

		blocks.regTileEntitys();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {

		jutil.print("Fully initialized.");
	}

}