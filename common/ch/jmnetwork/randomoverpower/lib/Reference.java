package ch.jmnetwork.randomoverpower.lib;

import net.minecraft.creativetab.CreativeTabs;
import ch.jmnetwork.randomoverpower.creativetab.ROCreativeTab;
import ch.jmnetwork.randomoverpower.util.JUtil;

public class Reference {

	public static final String MOD_ID = "randomOverpwr";
	public static final String MOD_NAME = "Random Overpower Mod";
	public static final String VERSION = "1.6b_dev";
	public static JUtil jutil = new JUtil(MOD_NAME);
	public static final CreativeTabs RO_CREATIVE_TAB = new ROCreativeTab(CreativeTabs.getNextID(), "Random Overpower");
	public static final int DIG_SIZE_SHOVEL_PICK = 1;
}
