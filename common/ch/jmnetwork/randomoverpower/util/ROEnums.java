package ch.jmnetwork.randomoverpower.util;

import net.minecraft.item.EnumToolMaterial;
import net.minecraftforge.common.EnumHelper;

public class ROEnums {

    public static EnumToolMaterial OVERPOWER = EnumHelper.addToolMaterial("OVERPOWER", 5, 10000, 45.0F, 0, 30);
    public static EnumToolMaterial OVERPOWER_SWORD = EnumHelper.addToolMaterial("OVERPOWER_SWORD", 0, 10000, 45.0F, 30, 30);
    public static EnumToolMaterial INSTAKILLER = EnumHelper.addToolMaterial("INSTAKILLER", 0, 1, 0F, 0, 0);

}
