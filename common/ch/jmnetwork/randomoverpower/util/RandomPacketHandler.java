package ch.jmnetwork.randomoverpower.util;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.Player;

public class RandomPacketHandler implements IPacketHandler {

	public RandomPacketHandler() {
		NetworkRegistry.instance().registerChannel(this, "RandomMod");
	}

	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {

		if (packet.channel.equals("RandomMod")) {
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(packet.data));

			double xPos, yPos, zPos;

			try {
				xPos = dis.readDouble();
				yPos = dis.readDouble();
				zPos = dis.readDouble();
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}

			World world = Minecraft.getMinecraft().thePlayer.worldObj;

			for (int i = 0; i < 10; i++) {
				float particleX = (float) (xPos + world.rand.nextFloat());
				float particleY = (float) (yPos + world.rand.nextFloat());
				float particleZ = (float) (zPos + world.rand.nextFloat());

				float particleMotionX = -0.5F + world.rand.nextFloat();
				float particleMotionY = -0.5F + world.rand.nextFloat();
				float particleMotionZ = -0.5F + world.rand.nextFloat();

				world.spawnParticle("portal", particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
				System.out.println("Spawned particle at" + particleX + ", " + particleY + ", " + particleZ + " (Motions: " + particleMotionX + ", " + particleMotionY + ", " + particleMotionZ);
			}
		}
	}
}
