package ch.jmnetwork.randomoverpower.util;

import java.io.File;
import ch.jmnetwork.randomoverpower.blocks.BlockReference;
import ch.jmnetwork.randomoverpower.items.ItemReference;
import net.minecraftforge.common.Configuration;


public class ConfigHandler {

	public ConfigHandler(File file) {

		Configuration config = new Configuration(file);

		config.load();

		ItemReference.HARDDIAMOND_ACTIVE = config.get("Items", "HARDDIAMOND_ACTIVE", true).getBoolean(true);

		if (ItemReference.HARDDIAMOND_ACTIVE) {

			ItemReference.HARDDIAMOND_CLUSTER_ACTIVE = config.get("Items", "HARDDIAMOND_CLUSTER_ACTIVE", true).getBoolean(true);

			if (ItemReference.HARDDIAMOND_CLUSTER_ACTIVE) {
				ItemReference.HARDDIAMOND_STICK_ACTIVE = config.get("Items", "HARDDIAMOND_STICK_ACTIVE", true).getBoolean(true);
			} else {
				ItemReference.HARDDIAMOND_STICK_ACTIVE = false;
			}

			if (ItemReference.HARDDIAMOND_STICK_ACTIVE) {

				ItemReference.OVERPOWER_PICK_ACTIVE = config.get("Items", "OVERPOWER_PICK_ACTIVE", true).getBoolean(true);
				ItemReference.OVERPOWER_SHOVEL_ACTIVE = config.get("Items", "OVERPOWER_SHOVEL_ACTIVE", true).getBoolean(true);
				ItemReference.OVERPOWER_AXE_ACTIVE = config.get("Items", "OVERPOWER_AXE_ACTIVE", true).getBoolean(true);
				ItemReference.OVERPOWER_SWORD_ACTIVE = config.get("Items", "OVERPOWER_SWORD_ACTIVE", true).getBoolean(true);
			} else {
				ItemReference.OVERPOWER_AXE_ACTIVE = false;
				ItemReference.OVERPOWER_PICK_ACTIVE = false;
				ItemReference.OVERPOWER_SHOVEL_ACTIVE = false;
				ItemReference.OVERPOWER_SWORD_ACTIVE = false;
			}
		} else {
			ItemReference.HARDDIAMOND_CLUSTER_ACTIVE = false;
			ItemReference.HARDDIAMOND_STICK_ACTIVE = false;
			ItemReference.OVERPOWER_AXE_ACTIVE = false;
			ItemReference.OVERPOWER_PICK_ACTIVE = false;
			ItemReference.OVERPOWER_SHOVEL_ACTIVE = false;
			ItemReference.OVERPOWER_SWORD_ACTIVE = false;
		}

		ItemReference.INSTAKILLER_ACTIVE = config.get("Items", "INSTAKILLER_ACTIVE", true).getBoolean(true);
		ItemReference.FINEWOOD_ACTIVE = config.get("Items", "FINEWOOD_ACTIVE", true).getBoolean(true);
		if (ItemReference.FINEWOOD_ACTIVE) ItemReference.OVEPOWER_FUEL_ACTIVE = config.get("Items", "OVERPOWER_FUEL_ACTIVE", true).getBoolean(true);
		ItemReference.PVOID_ACTIVE = config.get("Items", "PVOID_ACTIVE", true).getBoolean(true);

		BlockReference.GLOWCOBBLE_ACTIVE = config.get("Blocks", "GLOWCOBBLE_ACTIVE", true).getBoolean(true);
		BlockReference.MILL_ACTIVE = config.get("Blocks", "MILL_ACTIVE", true).getBoolean(true);
		if (BlockReference.GLOWCOBBLE_ACTIVE) BlockReference.SPEEDBLOCK_ACTIVE = config.get("Blocks", "SPEEDBLOCK_ACTIVE", true).getBoolean(true);
		else {
			BlockReference.SPEEDBLOCK_ACTIVE = false;
		}

		config.save();
	}
}
