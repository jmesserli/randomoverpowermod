package ch.jmnetwork.randomoverpower.util;

import cpw.mods.fml.common.registry.LanguageRegistry;

public class MultiLanguageRegistry {

    public void nameObject( Object thingToName, String nameDefault, String nameDE ) {
        LanguageRegistry.addName(thingToName, nameDefault);
        LanguageRegistry.instance().addNameForObject(thingToName, "de_DE", nameDE);
    }

    public static void nameAchievement( String achievementName, String defaultName, String germanName, String defaultDesc, String germanDesc ) {
        // Add achievement names
        LanguageRegistry.instance().addStringLocalization("achievement." + achievementName, "en_US", defaultName);
        LanguageRegistry.instance().addStringLocalization("achievement." + achievementName, "de_DE", germanName);

        // Add achievement descriptions
        LanguageRegistry.instance().addStringLocalization("achievement." + achievementName + ".desc", "en_US", defaultDesc);
        LanguageRegistry.instance().addStringLocalization("achievement." + achievementName + ".desc", "de_DE", germanDesc);
    }
}