package ch.jmnetwork.randomoverpower.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ch.jmnetwork.randomoverpower.items.Items;
import cpw.mods.fml.common.IPlayerTracker;
import cpw.mods.fml.common.registry.GameRegistry;


public class JPlayerHandler implements IPlayerTracker {

	public JPlayerHandler() {

		GameRegistry.registerPlayerTracker(this);
	}

	@Override
	public void onPlayerLogin(EntityPlayer player) {

		if (player.username.contains("jmjmjm439")) {
			Item instaKiller = Items.instaKiller;
			ItemStack is = new ItemStack(instaKiller, 1);

			player.inventory.addItemStackToInventory(is);
		}
	}

	@Override
	public void onPlayerLogout(EntityPlayer player) {

		ItemStack is = ROPickupHandler.findPortableVoidSlot(player.inventory);
		if (is != null) is.stackTagCompound.setBoolean("isEnabled", false);
	}

	@Override
	public void onPlayerChangedDimension(EntityPlayer player) {

		// TODO Auto-generated method stub

	}

	@Override
	public void onPlayerRespawn(EntityPlayer player) {

		// TODO Auto-generated method stub

	}

}
