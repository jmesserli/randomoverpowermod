package ch.jmnetwork.randomoverpower.util;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ch.jmnetwork.randomoverpower.lib.Reference;

public class JUtil {
	String modName = "";

	/**
	 * @param mname
	 *            Name of the Mod
	 */
	public JUtil(String mname) {
		modName = mname;
	}

	public int getShiftedIndex(int i) {
		return i + 256;
	}

	public void print(String stringToPrint) {
		System.out.println("[" + modName + "] " + stringToPrint);
	}

	public String getTextureString(String itemTextureFile) {
		return Reference.MOD_ID + ":" + itemTextureFile;
	}

	public int axisPlayerStandingToBlock(int blockX, int blockY, int blockZ, double playerX, double playerY, double playerZ) {

		double workX, workY, workZ = 0D;

		workX = abs(blockX - playerX);
		workY = abs(blockY - playerY);
		workZ = abs(blockZ - playerZ);

		int biggest = 0; // 1: X \ 2: Y \ 3: Z

		if (workX > workY && workX > workZ) {
			biggest = 1;
		} else if (workY > workX && workY > workZ) {
			biggest = 2;
		} else if (workZ > workX && workZ > workY) {
			biggest = 3;
		}

		return biggest;
	}

	public double abs(double toAbs) {
		return Math.abs(toAbs);
	}

	public void spawnItemInWorld(World world, ItemStack item, double x, double y, double z, int pickupTime, int lifetime) {

		EntityItem entityItem = new EntityItem(world, x, y, z, item);
		entityItem.delayBeforeCanPickup = pickupTime;
		
		if (lifetime != 0) {
			entityItem.lifespan = lifetime;
		} else {
			entityItem.lifespan = 6000;
		}

		world.spawnEntityInWorld(entityItem);

	}

}
