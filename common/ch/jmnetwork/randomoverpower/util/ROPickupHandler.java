package ch.jmnetwork.randomoverpower.util;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import ch.jmnetwork.randomoverpower.items.ItemPortableVoid;
import ch.jmnetwork.randomoverpower.items.ItemReference;
import ch.jmnetwork.randomoverpower.items.Items;
import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.common.network.PacketDispatcher;


public class ROPickupHandler {

	public ROPickupHandler() {

		MinecraftForge.EVENT_BUS.register(this);
	}

	@ForgeSubscribe
	public void notifyPickup(EntityItemPickupEvent event) {

		if (!event.item.worldObj.isRemote) {

			ItemStack portableVoid = findPortableVoidSlot(event.entityPlayer.inventory);

			if (portableVoid != null) {
				if (shouldItemBeVoided(event.item.getEntityItem(), (ItemPortableVoid) portableVoid.getItem(), portableVoid, event.item.worldObj)) {
					event.setCanceled(true);

					// ======================================//
					// SEND PARTICLE PACKET
					// ======================================//
					ByteArrayOutputStream outputBytes = new ByteArrayOutputStream();
					DataOutputStream dis = new DataOutputStream(outputBytes);

					try {

						dis.writeDouble(event.item.posX);
						dis.writeDouble(event.item.posY);
						dis.writeDouble(event.item.posZ);

						PacketDispatcher.sendPacketToAllPlayers(PacketDispatcher.getPacket("RandomMod", outputBytes.toByteArray()));
					} catch (Exception e) {
						e.printStackTrace();
						return;
					} // END PACKET SEND

					event.item.setDead();
				}
			}
		}
	}

	public static ItemStack findPortableVoidSlot(InventoryPlayer invPlayer) {

		for (int i = invPlayer.getSizeInventory() - 4; i >= 0; i--) {

			ItemStack is = invPlayer.getStackInSlot(i);

			if (is != null && is.itemID == Items.portableVoid.itemID) {
				// Void found in slot i
				return is;
			}
		}
		return null; // return null if no portable void was found
	}

	private boolean shouldItemBeVoided(ItemStack item, ItemPortableVoid itemPortableVoid, ItemStack itemPortableVoidItemStack, World world) {

		System.out.println("should item be voided?, isStorageValid: " + itemPortableVoid.isStorageValid(itemPortableVoidItemStack, world) + ", isEnabled : " + itemPortableVoid.isEnabled(itemPortableVoidItemStack));
		if (itemPortableVoid.isStorageValid(itemPortableVoidItemStack, world) && itemPortableVoid.isEnabled(itemPortableVoidItemStack)) {
			System.out.println("Storage valid, PVoid enabled");
			for (int i = 0; i < itemPortableVoid.getStorageInventory(itemPortableVoidItemStack, world).getSizeInventory(); i++) {
				System.out.println("itemID: " + item.itemID);
				System.out.println("StorageItemID: " + itemPortableVoid.getStorageInventory(itemPortableVoidItemStack, world).getStackInSlot(i) != null ? itemPortableVoid.getStorageInventory(itemPortableVoidItemStack, world).getStackInSlot(i).itemID : 0);
				if (itemPortableVoid.getStorageInventory(itemPortableVoidItemStack, world).getStackInSlot(i) != null && item.itemID == itemPortableVoid.getStorageInventory(itemPortableVoidItemStack, world).getStackInSlot(i).itemID) { return true; }
			}
		}
		return false;
	}
}