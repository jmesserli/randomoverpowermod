package ch.jmnetwork.randomoverpower.util;

import ch.jmnetwork.randomoverpower.items.Items;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import cpw.mods.fml.common.ICraftingHandler;
import cpw.mods.fml.common.registry.GameRegistry;

public class OPCraftHandler implements ICraftingHandler {

	public OPCraftHandler() {
		GameRegistry.registerCraftingHandler(this);
	}

	@Override
	public void onCrafting(EntityPlayer player, ItemStack item, IInventory craftMatrix) {
		if (item.itemID == Items.fineWood.itemID) {

			ItemStack datOne = craftMatrix.getStackInSlot(0);
			if (datOne != null) {
				ItemStack temp = new ItemStack(datOne.getItem(), (datOne.getItemDamage() + 2));

				if (temp.getItemDamage() >= temp.getMaxDamage()) {
					temp.stackSize--;
				}

				craftMatrix.setInventorySlotContents(0, temp);
			}

		}
	}

	@Override
	public void onSmelting(EntityPlayer player, ItemStack item) {
	}

}
