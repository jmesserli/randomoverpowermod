package ch.jmnetwork.randomoverpower.util;

import ch.jmnetwork.randomoverpower.items.ItemReference;
import ch.jmnetwork.randomoverpower.items.Items;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.IFuelHandler;
import cpw.mods.fml.common.registry.GameRegistry;

public class OPFuelHandler implements IFuelHandler {

	public OPFuelHandler() {
		GameRegistry.registerFuelHandler(this);
	}

	@Override
	public int getBurnTime(ItemStack fuel) {
		if (fuel.itemID == Items.overpowerFuel.itemID) {
			return ItemReference.OVERPOWER_FUEL_BURN_TIME;
		} else if (fuel.itemID == Items.fineWood.itemID) {
			return 33;
		}

		return 0;
	}
}
