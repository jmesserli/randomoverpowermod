package ch.jmnetwork.randomoverpower.items;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemOverpowerPick extends ItemPickaxe {

	public ItemOverpowerPick(int par1, EnumToolMaterial par2EnumToolMaterial) {
		super(par1, par2EnumToolMaterial);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
		setUnlocalizedName(ItemReference.OVERPOWER_PICK_UNLOCALIZED_NAME);
	}

	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add("\2476Tool Durability: \2474" + (10000 - par1ItemStack.getItemDamage()) + "\247f/\247410000");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.OVERPOWER_PICK_ICON));
	}

	@Override
	public boolean hasEffect(ItemStack par1ItemStack) {
		return true;
	}

	@Override
	public boolean onBlockDestroyed(ItemStack itemStack, World world, int par3, int x, int y, int z, EntityLivingBase entityLiving) {

		if (!world.isRemote) {
			EntityPlayer ep = (EntityPlayer) entityLiving;
			if (!ep.isSneaking()) {

				int axis = Reference.jutil.axisPlayerStandingToBlock(x, y, z, ep.posX, ep.posY, ep.posZ);

				int doX = Reference.DIG_SIZE_SHOVEL_PICK;
				int doY = Reference.DIG_SIZE_SHOVEL_PICK;
				int doZ = Reference.DIG_SIZE_SHOVEL_PICK;

				switch (axis) {
				case 0:
					break;
				case 1: // X biggest
					doX = 0;
					break;
				case 2: // Y biggest
					doY = 0;
					break;
				case 3: // Z biggest
					doZ = 0;
					break;
				}

				for (int X = -doX; X <= doX; X++) {
					for (int Y = -doY; Y <= doY; Y++) {
						for (int Z = -doZ; Z <= doZ; Z++) {

							if (X != 0 || Y != 0 || Z != 0) {

								Block block = Block.blocksList[world.getBlockId(x + X, y + Y, z + Z)];

								if (block != null) {

									ArrayList<ItemStack> list = block.getBlockDropped(world, x + X, y + Y, z + Z, 0, 10);
									for (ItemStack is : list) {
										Reference.jutil.spawnItemInWorld(world, is, ep.posX, ep.posY, ep.posZ, 0, 0);
									}

									world.setBlockToAir(x + X, y + Y, z + Z);
									itemStack.damageItem(1, entityLiving);
								}
							}
						}
					}
				}
			}
		}

		return super.onBlockDestroyed(itemStack, world, par3, x, y, z, entityLiving);
	}
}
