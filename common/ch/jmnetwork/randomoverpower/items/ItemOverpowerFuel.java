package ch.jmnetwork.randomoverpower.items;

import java.util.List;

import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemOverpowerFuel extends Item {

	public ItemOverpowerFuel(int par1) {
		super(par1);
		setUnlocalizedName(ItemReference.OVERPOWER_FUEL_UNLOCALIZED_NAME);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.OVERPOWER_FUEL_ICON));
	}

	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add("Can burn 50 items!");
	}

}
