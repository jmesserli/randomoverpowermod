package ch.jmnetwork.randomoverpower.items;

import ch.jmnetwork.randomoverpower.lib.Reference;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemHardDiamondCluster extends Item {

	public ItemHardDiamondCluster(int par1) {
		super(par1);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
		setUnlocalizedName(ItemReference.HARDDIAMOND_CLUSTER_UNLOCALIZED_NAME);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.HARDDIAMOND_CLUSER_ICON));
	}

}
