package ch.jmnetwork.randomoverpower.items;

import cpw.mods.fml.common.registry.GameRegistry;
import ch.jmnetwork.randomoverpower.recipes.FineWoodRecipe;
import ch.jmnetwork.randomoverpower.util.MultiLanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;


public class Items {

	MultiLanguageRegistry mlr = new MultiLanguageRegistry();

	Item overpowerPick;
	public static Item hardDiamond;
	Item hardDiamondStick;
	Item hardDiamondCluster;
	Item overpowerShovel;
	Item overpowerSword;
	public static Item overpowerAxe;
	public static Item overpowerFuel;
	public static Item fineWood;
	public static Item instaKiller;
	public static Item portableVoid;

	public void init() {

		hardDiamondStick = ItemReference.HARDDIAMOND_STICK_ACTIVE == true ? new ItemHardDiamondStick(ItemReference.HARDDIAMOND_STICK_DEFAULT_ID) : null;
		hardDiamond = ItemReference.HARDDIAMOND_ACTIVE == true ? new ItemHardenedDiamond(ItemReference.HARDDIAMOND_DEFAULT_ID) : null;
		hardDiamondCluster = ItemReference.HARDDIAMOND_CLUSTER_ACTIVE == true ? new ItemHardDiamondCluster(ItemReference.HARDDIAMOND_CLUSTER_DEFAULT_ID) : null;

		overpowerPick = ItemReference.OVERPOWER_PICK_ACTIVE == true ? new ItemOverpowerPick(ItemReference.OVERPOWER_PICK_DEFAULT_ID, ItemReference.OVERPOWER_PICK_MATERIAL) : null;
		overpowerShovel = ItemReference.OVERPOWER_SHOVEL_ACTIVE == true ? new ItemOverpowerShovel(ItemReference.OVERPOWER_SHOVEL_DEFAULT_ID, ItemReference.OVEPOWER_SHOVEL_MATERIAL) : null;
		overpowerSword = ItemReference.OVERPOWER_SWORD_ACTIVE == true ? new ItemOverpowerSword(ItemReference.OVERPOWER_SWORD_DEFAULT_ID, ItemReference.OVERPOWER_SWORD_MATERIAL) : null;
		overpowerAxe = ItemReference.OVERPOWER_AXE_ACTIVE == true ? new ItemOverpowerAxe(ItemReference.OVERPOWER_AXE_DEFAULT_ID, ItemReference.OVERPOWER_PICK_MATERIAL) : null;
		instaKiller = ItemReference.INSTAKILLER_ACTIVE == true ? new ItemInstaKiller(ItemReference.INSTAKILLER_DEFAULT_ID, ItemReference.INSTAKILLER_TOOL_MATERIAL) : null;

		fineWood = ItemReference.FINEWOOD_ACTIVE == true ? new ItemFineWood(ItemReference.FINEWOOD_DEFAULT_ID) : null;
		overpowerFuel = ItemReference.OVEPOWER_FUEL_ACTIVE == true ? new ItemOverpowerFuel(ItemReference.OVERPOWER_FUEL_DEFAULT_ID) : null;
		portableVoid = ItemReference.PVOID_ACTIVE == true ? new ItemPortableVoid(ItemReference.PVOID_DEFAULT_ID) : null;
	}

	public void addNames() {

		if (overpowerPick != null) {
			mlr.nameObject(overpowerPick, ItemReference.OVERPOWER_PICK_NAME_EN, ItemReference.OVERPOWER_PICK_NAME_DE);
		}
		if (hardDiamond != null) {
			mlr.nameObject(hardDiamond, ItemReference.HARDDIAMOND_NAME_EN, ItemReference.HARDDIAMOND_NAME_DE);
		}
		if (hardDiamondStick != null) {
			mlr.nameObject(hardDiamondStick, ItemReference.HARDDIAMOND_STICK_NAME_EN, ItemReference.HARDDIAMOND_STICK_NAME_DE);
		}
		if (hardDiamondCluster != null) {
			mlr.nameObject(hardDiamondCluster, ItemReference.HARDDIAMOND_CLUSTER_NAME_EN, ItemReference.HARDDIAMOND_CLUSTER_NAME_DE);
		}
		if (overpowerShovel != null) {
			mlr.nameObject(overpowerShovel, ItemReference.OVERPOWER_SHOVEL_NAME_EN, ItemReference.OVERPOWER_SHOVEL_NAME_DE);
		}
		if (overpowerSword != null) {
			mlr.nameObject(overpowerSword, ItemReference.OVERPOWER_SWORD_NAME_EN, ItemReference.OVERPOWER_SWORD_NAME_DE);
		}
		if (overpowerAxe != null) {
			mlr.nameObject(overpowerAxe, ItemReference.OVERPOWER_AXE_NAME_EN, ItemReference.OVERPOWER_AXE_NAME_DE);
		}
		if (instaKiller != null) {
			mlr.nameObject(instaKiller, ItemReference.INSTAKILLER_NAME_EN, ItemReference.INSTAKILLER_NAME_DE);
		}
		if (overpowerFuel != null) {
			mlr.nameObject(overpowerFuel, ItemReference.OVERPOWER_FUEL_NAME_EN, ItemReference.OVERPOWER_FUEL_NAME_DE);
		}
		if (fineWood != null) {
			mlr.nameObject(fineWood, ItemReference.FINEWOOD_NAME_EN, ItemReference.FINEWOOD_NAME_DE);
		}
		if (portableVoid != null) {
			mlr.nameObject(portableVoid, ItemReference.PVOID_NAME_EN, ItemReference.PVOID_NAME_DE);
		}
	}

	public void addRecipes() {

		if (hardDiamond != null) {
			GameRegistry.addSmelting(Item.diamond.itemID, new ItemStack(hardDiamond, 1), 1000);
			GameRegistry.addShapelessRecipe(new ItemStack(Item.diamond, 1), new ItemStack(hardDiamond, 1));
		}
		if (hardDiamondStick != null) {
			GameRegistry.addRecipe(new ItemStack(hardDiamondStick, 4), new Object[] { "D  ", "D  ", "   ", 'D', hardDiamondCluster });
		}
		if (overpowerPick != null) {
			GameRegistry.addRecipe(new ItemStack(overpowerPick, 1), new Object[] { "GGG", " S ", " S ", 'G', hardDiamond, 'S', hardDiamondStick });
		}
		if (hardDiamondCluster != null) {
			GameRegistry.addShapelessRecipe(new ItemStack(hardDiamondCluster, 1), new ItemStack(hardDiamond, 1), new ItemStack(hardDiamond, 1), new ItemStack(hardDiamond, 1), new ItemStack(hardDiamond, 1));
		}
		if (overpowerShovel != null) {
			GameRegistry.addRecipe(new ItemStack(overpowerShovel, 1), new Object[] { " D ", " S ", " S ", 'D', hardDiamond, 'S', hardDiamondStick });
		}
		if (overpowerSword != null) {
			GameRegistry.addRecipe(new ItemStack(overpowerSword, 1), new Object[] { " D ", " D ", " S ", 'D', hardDiamond, 'S', hardDiamondStick });
		}
		if (overpowerAxe != null) {
			GameRegistry.addRecipe(new ItemStack(overpowerAxe, 1), new Object[] { "DD ", "DS ", " S ", 'D', hardDiamond, 'S', hardDiamondStick });
		}
		if (overpowerFuel != null) {
			GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(overpowerFuel, 1), new Object[] { "FFF", "LLL", "CCC", 'F', fineWood, 'L', "logWood", 'C', Item.coal }));
		}
		if (fineWood != null) {
			GameRegistry.addRecipe(new FineWoodRecipe());
			GameRegistry.addRecipe(new ItemStack(Block.wood, 1), new Object[] { "FFF", "FFF", "FFF", 'F', fineWood });
		}
		if (hardDiamond != null && hardDiamondCluster != null) {
			GameRegistry.addShapelessRecipe(new ItemStack(hardDiamond, 4), new ItemStack(hardDiamondCluster, 1));
		}
		if (portableVoid != null) {
			GameRegistry.addRecipe(new ItemStack(portableVoid, 1), new Object[] { "EOE", "OEO", "EOE", 'E', Item.enderPearl, 'O', Block.obsidian });
		}
	}
}