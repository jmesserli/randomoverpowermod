package ch.jmnetwork.randomoverpower.items;

import java.util.List;

import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;

public class ItemOverpowerAxe extends ItemAxe {

	public ItemOverpowerAxe(int par1, EnumToolMaterial par2EnumToolMaterial) {
		super(par1, par2EnumToolMaterial);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
		setUnlocalizedName(ItemReference.OVERPOWER_AXE_UNLOCALIZED_NAME);
	}

	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add("\2476Tool Durability: \2474" + (10000 - par1ItemStack.getItemDamage()) + "\247f/\247410000");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.OVERPOWER_AXE_ICON));
	}
}
