package ch.jmnetwork.randomoverpower.items;

import java.util.List;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import ch.jmnetwork.randomoverpower.RandomOverpowerMod;
import ch.jmnetwork.randomoverpower.blocks.tileentitys.TileEntityPVoidStorage;
import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;


public class ItemPortableVoid extends Item {

	private long lastTime = System.nanoTime();
	private long thisTime = 0;

	public ItemPortableVoid(int id) {

		super(id);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
		setUnlocalizedName(ItemReference.PVOID_UNLOCALIZED_NAME);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer) {

		if (itemStack.stackTagCompound == null) {
			onCreated(itemStack, world, entityPlayer);
		}

		if (!entityPlayer.isSneaking() && isStorageValid(itemStack, world)) {

			entityPlayer.openGui(RandomOverpowerMod.INSTANCE, 1, world, itemStack.stackTagCompound.getIntArray("storageCoords")[0], itemStack.stackTagCompound.getIntArray("storageCoords")[1], itemStack.stackTagCompound.getIntArray("storageCoords")[2]);
		} else {
			thisTime = System.nanoTime();
			double seconds = (double) ((thisTime - lastTime) / 1000000000.0D);

			if (seconds >= 1) {

				if (isStorageValid(itemStack, world)) {
					itemStack.stackTagCompound.setBoolean("isEnabled", itemStack.stackTagCompound.getBoolean("isEnabled") == true ? false : true);
					lastTime = System.nanoTime();
				} else {
					itemStack.stackTagCompound.setBoolean("isEnabled", false);
				}
			}
		}
		return super.onItemRightClick(itemStack, world, entityPlayer);
	}

	public boolean isLinkedToStorage(ItemStack is) {

		try {

			if (is.stackTagCompound != null && is.stackTagCompound.getIntArray("storageCoords") != null) {
				int i = is.stackTagCompound.getIntArray("storageCoords")[0];
				return true;
			} else {
				return false;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	public boolean isStorageValid(ItemStack is, World world) {

		if (!isLinkedToStorage(is)) { return false; }
		int x = is.stackTagCompound.getIntArray("storageCoords")[0], y = is.stackTagCompound.getIntArray("storageCoords")[1], z = is.stackTagCompound.getIntArray("storageCoords")[2];
		TileEntity tepvs = world.getBlockTileEntity(x, y, z);

		return (tepvs != null && tepvs instanceof TileEntityPVoidStorage);
	}

	public IInventory getStorageInventory(ItemStack itemStack, World world) {

		if (isStorageValid(itemStack, world)) {
			int storagecoords[] = itemStack.stackTagCompound.getIntArray("storageCoords");
			int x = itemStack.stackTagCompound.getIntArray("storageCoords")[0], y = itemStack.stackTagCompound.getIntArray("storageCoords")[1], z = itemStack.stackTagCompound.getIntArray("storageCoords")[2];

			return (TileEntityPVoidStorage) world.getBlockTileEntity(x, y, z);
		}
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {

		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.PVOID_ICON));
	}

	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean par4) {

		if (itemStack.stackTagCompound != null) {
			if (isStorageValid(itemStack, entityPlayer.worldObj)) {

				list.add(EnumChatFormatting.DARK_AQUA + "Linked to storage block at " + itemStack.stackTagCompound.getIntArray("storageCoords")[0] + ", " + itemStack.stackTagCompound.getIntArray("storageCoords")[1] + ", " + itemStack.stackTagCompound.getIntArray("storageCoords")[2]);
			} else {
				list.add(EnumChatFormatting.DARK_RED + "Not linked to storage block");
			}
			if (itemStack.stackTagCompound.getString("owner").equals(entityPlayer.username)) {
				list.add(EnumChatFormatting.GREEN + "Owner: " + entityPlayer.username);
			} else {
				list.add(EnumChatFormatting.RED + "Owner: " + itemStack.stackTagCompound.getString("owner"));
			}
		} else {
			list.add(EnumChatFormatting.GOLD + "A neat little hole in your pocket...");
			list.add(EnumChatFormatting.AQUA + "Owner: " + entityPlayer.username);
		}
	}

	@Override
	public boolean hasEffect(ItemStack itemStack) {

		if (itemStack.stackTagCompound != null) {
			return itemStack.stackTagCompound.getBoolean("isEnabled");
		} else {
			return true;
		}

	}

	public boolean isEnabled(ItemStack itemstack) {

		return itemstack.stackTagCompound.getBoolean("isEnabled");
	}

	@Override
	public void onCreated(ItemStack itemstack, World world, EntityPlayer entityPlayer) {

		itemstack.stackTagCompound = new NBTTagCompound();
		itemstack.stackTagCompound.setString("owner", entityPlayer.username);
		itemstack.stackTagCompound.setBoolean("isEnabled", false);
	}

	@Override
	public void onUpdate(ItemStack itemStack, World world, Entity entity, int par4, boolean par5) {

		if (itemStack.stackTagCompound == null) {
			if (entity instanceof EntityPlayer) {
				onCreated(itemStack, world, (EntityPlayer) entity);
			}
		}
	}
}
