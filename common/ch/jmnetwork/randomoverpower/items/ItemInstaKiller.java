package ch.jmnetwork.randomoverpower.items;

import java.util.List;
import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;


public class ItemInstaKiller extends ItemSword {

	protected final int COOLDOWN_TIME_SECS = 2;
	protected final double COOLDOWN_TIME_MS = COOLDOWN_TIME_SECS * 1e+3;

	public ItemInstaKiller(int par1, EnumToolMaterial par2EnumToolMaterial) {

		super(par1, par2EnumToolMaterial);
		setCreativeTab(CreativeTabs.tabAllSearch);
		setUnlocalizedName(ItemReference.INSTAKILLER_UNLOCALIZED_NAME);
	}

	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {

		par3List.add("\247CDeveloper Only!");
		par3List.add("This weapon is overly overpowered.");
	}

	@Override
	public boolean hasEffect(ItemStack par1ItemStack) {

		return true;
	}

	@Override
	public boolean itemInteractionForEntity(ItemStack itemstack, EntityPlayer player, EntityLivingBase entity) {

		World world = entity.worldObj;

		if (!world.isRemote) {
			if (player.username.contains("jmjmjm439")) {
				entity.attackEntityFrom(DamageSource.magic, 100000);
			}
		}

		return true;
	}

	@Override
	public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {

		if (par3Entity instanceof EntityPlayer) {
			if (!((EntityPlayer) par3Entity).username.contains("jmjmjm439")) {
				par1ItemStack.stackSize = 0;
			}
		}
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {

		if (par3EntityPlayer.username.contains("jmjmjm439")) {
			par3EntityPlayer.motionX *= 5;
			par3EntityPlayer.motionY *= 5;
			par3EntityPlayer.motionZ *= 5;
		}
		return par1ItemStack;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {

		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.INSTAKILLER_ICON));
	}
}
