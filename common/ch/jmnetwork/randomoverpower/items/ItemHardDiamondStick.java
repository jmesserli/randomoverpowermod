package ch.jmnetwork.randomoverpower.items;

import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemHardDiamondStick extends Item {

	public ItemHardDiamondStick(int par1) {
		super(par1);
		setUnlocalizedName(ItemReference.HARDDIAMOND_STICK_UNLOCALIZED_NAME);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.HARDDIAMOND_STICK_ICON));
	}
}
