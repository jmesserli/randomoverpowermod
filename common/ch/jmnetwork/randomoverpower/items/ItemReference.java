package ch.jmnetwork.randomoverpower.items;

import net.minecraft.item.EnumToolMaterial;
import ch.jmnetwork.randomoverpower.lib.Reference;
import ch.jmnetwork.randomoverpower.util.ROEnums;

public class ItemReference {
	//@formatter:off
    /*
    public static final String _NAME_EN = "";
    public static final String _NAME_DE = "";
    public static final int _DEFAULT_ID = 0;
    public static final String _UNLOCALIZED_NAME = "";
    public static Boolean _ACTIVE = true;
    */
    //@formatter:on

	public static final String TEXTURE_LOC = Reference.MOD_ID;

	// OVERPOWER PICKAXE
	public static final String OVERPOWER_PICK_NAME_EN = "Overpower Pickaxe";
	public static final String OVERPOWER_PICK_NAME_DE = "Overpower Picke";
	public static final int OVERPOWER_PICK_DEFAULT_ID = 1001;
	public static final String OVERPOWER_PICK_UNLOCALIZED_NAME = "overpowerPick";
	public static final EnumToolMaterial OVERPOWER_PICK_MATERIAL = ROEnums.OVERPOWER;
	public static final String OVERPOWER_PICK_ICON = "overpowerPick";
	public static Boolean OVERPOWER_PICK_ACTIVE = true;

	// HARDENED DIAMOND
	public static final String HARDDIAMOND_NAME_EN = "Hardened Diamond";
	public static final String HARDDIAMOND_NAME_DE = "Gehaerteter Diamant";
	public static final int HARDDIAMOND_DEFAULT_ID = 1002;
	public static final String HARDDIAMOND_UNLOCALIZED_NAME = "hardDiamond";
	public static final String HARDDIAMOND_ICON = "hardDiamond";
	public static Boolean HARDDIAMOND_ACTIVE = true;

	// HARDENED DIAMOND STICK
	public static final String HARDDIAMOND_STICK_NAME_EN = "Hardened Diamond Stick";
	public static final String HARDDIAMOND_STICK_NAME_DE = "Gehaerteter Diamantenstab";
	public static final int HARDDIAMOND_STICK_DEFAULT_ID = 1003;
	public static final String HARDDIAMOND_STICK_UNLOCALIZED_NAME = "hardDiamondStick";
	public static final String HARDDIAMOND_STICK_ICON = "hardDiamondStick";
	public static Boolean HARDDIAMOND_STICK_ACTIVE = true;

	// HARDENED DIAMOND CLUSTER
	public static final String HARDDIAMOND_CLUSTER_NAME_EN = "Hardened Dimond Cluster";
	public static final String HARDDIAMOND_CLUSTER_NAME_DE = "Gehaertetes Diamantenkluster";
	public static final int HARDDIAMOND_CLUSTER_DEFAULT_ID = 1004;
	public static final String HARDDIAMOND_CLUSTER_UNLOCALIZED_NAME = "hardDiamondCluster";
	public static final String HARDDIAMOND_CLUSER_ICON = "hardDiamondCluster";
	public static Boolean HARDDIAMOND_CLUSTER_ACTIVE = true;

	// OVERPOWER SHOVEL
	public static final String OVERPOWER_SHOVEL_NAME_EN = "Overpower Shovel";
	public static final String OVERPOWER_SHOVEL_NAME_DE = "Overpower Schaufel";
	public static final int OVERPOWER_SHOVEL_DEFAULT_ID = 1005;
	public static final String OVERPOWER_SHOVEL_UNLOCALIZED_NAME = "hardDiamondShovel";
	public static final EnumToolMaterial OVEPOWER_SHOVEL_MATERIAL = ROEnums.OVERPOWER;
	public static final String OVERPOWER_SHOVEL_ICON = "hardDiamondShovel";
	public static Boolean OVERPOWER_SHOVEL_ACTIVE = true;

	// OVERPOWER SWORD
	public static final String OVERPOWER_SWORD_NAME_EN = "Overpower Sword";
	public static final String OVERPOWER_SWORD_NAME_DE = "Overpower Schwert";
	public static final int OVERPOWER_SWORD_DEFAULT_ID = 1006;
	public static final String OVERPOWER_SWORD_UNLOCALIZED_NAME = "hardDiamondSword";
	public static final EnumToolMaterial OVERPOWER_SWORD_MATERIAL = ROEnums.OVERPOWER_SWORD;
	public static final String OVERPOWER_SWORD_ICON = "hardDiamondSword";
	public static Boolean OVERPOWER_SWORD_ACTIVE = true;

	// OVERPOWER BOW
	// public static final String OVERPOWER_BOW_NAME_EN = "Overpower Bow";
	// public static final String OVERPOWER_BOW_NAME_DE = "Overpower Bogen";
	// public static final int OVERPOWER_BOW_DEFAULT_ID = 1007;
	// public static final String OVERPOWER_BOW_UNLOCALIZED_NAME =
	// "randomOverpwr:hardDiamondBow";
	// public static final int OVERPOWER_BOW_MAX_USE_DURATION = 12000;

	// OVERPOWER AXE
	public static final String OVERPOWER_AXE_NAME_EN = "Overpower Axe";
	public static final String OVERPOWER_AXE_NAME_DE = "Overpower Axt";
	public static final int OVERPOWER_AXE_DEFAULT_ID = 1008;
	public static final String OVERPOWER_AXE_UNLOCALIZED_NAME = "hardDiamondAxe";
	public static final String OVERPOWER_AXE_ICON = "hardDiamondAxe";
	public static Boolean OVERPOWER_AXE_ACTIVE = true;

	// INSTAKILLER
	public static final String INSTAKILLER_NAME_EN = "\2474\247lInstaKiller";
	public static final String INSTAKILLER_NAME_DE = "\2474\247lInstaKiller";
	public static final int INSTAKILLER_DEFAULT_ID = 1009;
	public static final String INSTAKILLER_UNLOCALIZED_NAME = "instaKiller";
	public static final EnumToolMaterial INSTAKILLER_TOOL_MATERIAL = ROEnums.INSTAKILLER;
	public static final String INSTAKILLER_ICON = "instaKiller";
	public static Boolean INSTAKILLER_ACTIVE = true;

	// OVERPOWER FUEL
	public static final String OVERPOWER_FUEL_NAME_EN = "Overpower Fuel";
	public static final String OVERPOWER_FUEL_NAME_DE = "Overpower Brennstoff";
	public static final int OVERPOWER_FUEL_DEFAULT_ID = 1010;
	public static final String OVERPOWER_FUEL_UNLOCALIZED_NAME = "opFuel";
	public static final String OVERPOWER_FUEL_ICON = "opFuel";
	public static final int OVERPOWER_FUEL_BURN_TIME = (50 * 200);
	public static Boolean OVEPOWER_FUEL_ACTIVE = true;

	// FINE WOOD
	public static final String FINEWOOD_NAME_EN = "Fine Wood";
	public static final String FINEWOOD_NAME_DE = "Feines Holz";
	public static final int FINEWOOD_DEFAULT_ID = 1011;
	public static final String FINEWOOD_UNLOCALIZED_NAME = "fineWood";
	public static final String FINEWOOD_ICON = "fineWood";
	public static Boolean FINEWOOD_ACTIVE = true;

	// PORTABLE VOID
	public static final String PVOID_NAME_EN = "Portable Void";
	public static final String PVOID_NAME_DE = "Tragbares Schwarzes Loch";
	public static final int PVOID_DEFAULT_ID = 1012;
	public static final String PVOID_UNLOCALIZED_NAME = "portableVoid";
	public static final String PVOID_ICON = "portableVoid";
	public static Boolean PVOID_ACTIVE = true;
}
