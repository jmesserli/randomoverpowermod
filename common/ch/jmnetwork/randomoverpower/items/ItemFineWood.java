package ch.jmnetwork.randomoverpower.items;

import ch.jmnetwork.randomoverpower.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemFineWood extends Item {

	public ItemFineWood(int par1) {
		super(par1);
		setUnlocalizedName(ItemReference.FINEWOOD_UNLOCALIZED_NAME);
		setCreativeTab(Reference.RO_CREATIVE_TAB);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.FINEWOOD_ICON));
	}

}
