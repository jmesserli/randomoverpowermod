package ch.jmnetwork.randomoverpower.world;

import java.util.Random;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import ch.jmnetwork.randomoverpower.blocks.BlockReference;
import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;

public class JWorldGenerator implements IWorldGenerator {

	private WorldGenerator glowingCobbleGen;

	public JWorldGenerator() {
		GameRegistry.registerWorldGenerator(this);
		glowingCobbleGen = new WorldGenMinable(BlockReference.GLOWCOBBLE_DEFAULT_ID, 10);
	}

	private void generateStandardOre(Random rand, int chunkX, int chunkZ, World world, int iterations, WorldGenerator gen, int highestY, int lowestY) {
		for (int i = 0; i < iterations; i++) {

			int x = chunkX * 16 + rand.nextInt(16);
			int y = rand.nextInt(highestY - lowestY) + lowestY;
			int z = chunkZ * 16 + rand.nextInt(16);

			gen.generate(world, rand, x, y, z);
		}
	}

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		generateStandardOre(random, chunkX, chunkZ, world, 20, glowingCobbleGen, 128, 0);
	}

}
